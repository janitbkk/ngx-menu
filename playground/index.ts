/**
 * This is only for local test
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { NavbarModule }  from 'ngx-navbar';

@Component({
  selector: 'app',
  template: `sss`
})
class AppComponent {}

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [ AppComponent ],
  imports: [ BrowserModule, NavbarModule, BrowserAnimationsModule ]
})
class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule);
