import {
	MenuItem,
	InternalLink,
	ExternalLink,
	EmitValue,
	TriggerFunction,
	Actions,
	SvgIcon,
	MatIcon,
	Icons
} from '../models/index';


// ACTIONS
export function isInternalLink(action: Actions) {
	return actionExists(action) && action instanceof InternalLink;
}

export function isExternalLink(action: Actions) {
	return actionExists(action) && action instanceof ExternalLink;
}

export function isEmitValue(action: Actions) {
	return actionExists(action) && action instanceof EmitValue;
}

export function isTriggerFunction(action: Actions) {
	return actionExists(action) && action instanceof TriggerFunction;
}

export function actionExists(action: Actions): boolean {
	return action ? true : false;
}

// Defaults to internalLink
export function getAction(item: MenuItem): Actions {
	if (!actionExists) {
		return null;
	}
	if (typeof item.action === "string") {
		return new InternalLink(item.action); // DEFAULT
	}
	if (item.action instanceof Object) {
		return item.action;
	}
}
//

// ICONS
export function isSvgIcon(icon: Icons) {
	return iconExists(icon) && icon instanceof SvgIcon;
}

export function isMatIcon(icon: Icons) {
	return iconExists(icon) && icon instanceof MatIcon;
}

export function iconExists(icon: Icons): boolean {
	return icon ? true : false;
}

// defaults to svgIcon
export function getIcon(item: MenuItem) {
	if (!iconExists) {
		return null;
	}
	if (typeof item.icon === "string") {
		return new SvgIcon(item.icon); // DEFAULT;
	}
	if (item.icon instanceof Object) {
		return item.icon;
	}
}