// ACTIONS
const INTERNAL_LINK = 'internalLink';
const EXTERNAL_LINK = 'externalLink';
const EMIT_VALUE = 'emitValue';
const FUNCTION = 'triggerFunction';

export class InternalLink {
	readonly type = INTERNAL_LINK;
	constructor(public url: string) {}
}

export class ExternalLink {
	readonly type = EXTERNAL_LINK;
	constructor(public url: string) {}
}

export class EmitValue {
	readonly type = EMIT_VALUE;
	constructor(public value: any) {}
}

export class TriggerFunction {
	readonly type = FUNCTION;
	constructor(public value: Function) {}
}
//

export type Actions = EmitValue | InternalLink | ExternalLink | TriggerFunction;

// ICONS
const SVG_ICON = 'svgIcon';
const MAT_ICON = 'matIcon';

export class SvgIcon {
	readonly type = SVG_ICON;
	constructor(public value: string) {}
}

export class MatIcon {
	readonly type = MAT_ICON;
	constructor(public value: string) {}
}

export type Icons = SvgIcon | MatIcon;

export interface MenuItem {
	label?: string;
	action?: string | Actions;
	icon?: string | Icons;
	items?: any;
}

export type MenuItems = MenuItem[];