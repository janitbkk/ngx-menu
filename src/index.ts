import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { 
  MatMenuModule,
  MatIconModule,
  MatButtonModule,
  MatExpansionModule,
  MatListModule
} from '@angular/material';
const MODULES = [
  CommonModule,
  RouterModule,
  MatMenuModule,
  MatIconModule,
  MatButtonModule,
  MatExpansionModule,
  MatListModule
];

export * from './models/index';
export * from './helpers/index';

import { EmitValueService } from './services/emit-value.service';
export { EmitValueService } from './services/emit-value.service';

import { MenuMobileComponent } from './components/menu-mobile/menu-mobile.component';
export { MenuMobileComponent } from './components/menu-mobile/menu-mobile.component';

import { MenuSidenavComponent } from './components/menu-sidenav/menu-sidenav.component';
export { MenuSidenavComponent } from './components/menu-sidenav/menu-sidenav.component';

import { MenuSidenavInnerComponent } from './components/menu-sidenav-inner/menu-sidenav-inner.component';
export { MenuSidenavInnerComponent } from './components/menu-sidenav-inner/menu-sidenav-inner.component';

import { MenuHorizontalComponent } from './components/menu-horizontal/menu-horizontal.component';
export { MenuHorizontalComponent } from './components/menu-horizontal/menu-horizontal.component';

import { MenuDropdownComponent } from './components/menu-dropdown/menu-dropdown.component';
export { MenuDropdownComponent } from './components/menu-dropdown/menu-dropdown.component';

import { MenuDropdownItemComponent } from './components/menu-dropdown-item/menu-dropdown-item.component';
export { MenuDropdownItemComponent } from './components/menu-dropdown-item/menu-dropdown-item.component';

import { MenuBtnComponent } from './components/menu-btn/menu-btn.component';
export { MenuBtnComponent } from './components/menu-btn/menu-btn.component';

import { MenuBtnInnerComponent } from './components/menu-btn-inner/menu-btn-inner.component';
export { MenuBtnInnerComponent } from './components/menu-btn-inner/menu-btn-inner.component';

const COMPONENTS = [
  MenuMobileComponent,
  MenuSidenavComponent,
  MenuSidenavInnerComponent,
  MenuHorizontalComponent,
  MenuDropdownComponent,
  MenuDropdownItemComponent,
  MenuBtnComponent,
  MenuBtnInnerComponent
];

@NgModule({
  imports: [ ...MODULES ],
  declarations: [ ...COMPONENTS ],
  exports: [
    ...MODULES,
    ...COMPONENTS
  ],
  providers: [ EmitValueService ]
})
export class MenuModule {}
