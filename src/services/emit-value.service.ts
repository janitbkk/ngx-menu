import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable() 
export class EmitValueService {
	private _value: BehaviorSubject<any> = new BehaviorSubject({});
	readonly value: Observable<any> = this._value.asObservable();
	
    emit(value: any) {
        this._value.next(value);
    }
}
