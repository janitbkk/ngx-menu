import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EmitValueService } from '../../services/emit-value.service';
import { MenuItems } from '../../models/index';

 @Component({
	selector: 'cdl-menu-mobile',
	templateUrl: './menu-mobile.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
 })
 export class MenuMobileComponent implements OnInit {
	private ngUnsubscribe: Subject<any> = new Subject();
	@Input() items: MenuItems;
	@Output() onEmitValue: EventEmitter<any> = new EventEmitter();

	constructor(
		private emitValueService: EmitValueService
	) {}

	ngOnInit() {
		this.emitValueService.value
			.pipe(takeUntil(this.ngUnsubscribe))
			.subscribe(value => this.onEmitValue.emit(value));
	}

	ngOnDestroy() {
		this.ngUnsubscribe.next();
		this.ngUnsubscribe.complete();
  	}

 }
 