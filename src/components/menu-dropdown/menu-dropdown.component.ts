import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
	selector: 'cdl-menu-dropdown',
	templateUrl: './menu-dropdown.component.html',
	styleUrls: ['./menu-dropdown.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuDropdownComponent {
	@Input() item: any; 
	@Input() isNested: boolean = false;
}
 