import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { EmitValueService } from '../../services/emit-value.service';
import { MenuItems } from '../../models/index';

 @Component({
	selector: 'cdl-menu-sidenav',
	templateUrl: './menu-sidenav.component.html',
	styleUrls: ['./menu-sidenav.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
 })
 export class MenuSidenavComponent implements OnInit {
	private ngUnsubscribe: Subject<any> = new Subject();
	@Input() items: MenuItems;
	@Input() expandedItems: number[] = [];
	@Output() onEmitValue: EventEmitter<any> = new EventEmitter();

	sidenavOpen: boolean = false;

	constructor(
		private emitValueService: EmitValueService,
		private router: Router,	
		private changeDetector: ChangeDetectorRef
	) {}

	ngOnInit() {
		this.emitValueService.value
			.pipe(takeUntil(this.ngUnsubscribe))
			.subscribe(value => this.onEmitValue.emit(value));

		this.router.events
			.pipe(
				takeUntil(this.ngUnsubscribe),
				filter(evt => evt instanceof NavigationEnd))
			.subscribe(evt => {
				this.close();
				this.changeDetector.markForCheck();
			});
	}

	ngOnDestroy() {
		this.ngUnsubscribe.next();
		this.ngUnsubscribe.complete();
	}
	  
	toggle() {
		this.sidenavOpen = !this.sidenavOpen;
	}

	open() {
		this.sidenavOpen = true;
	}

	close() {
		this.sidenavOpen = false;
	}

	isExpanded(index: number) {
		return this.expandedItems.indexOf(index) > -1;
	}

 }
 