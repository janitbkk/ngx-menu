import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EmitValueService } from '../../services/emit-value.service';
import { MenuItems } from '../../models/index';

 @Component({
	selector: 'cdl-menu-sidenav-inner',
	templateUrl: './menu-sidenav-inner.component.html',
	styleUrls: ['./menu-sidenav-inner.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
 })
 export class MenuSidenavInnerComponent implements OnInit {
	private ngUnsubscribe: Subject<any> = new Subject();
	@Input() items: MenuItems;
	@Input() expandedItems: number[] = [];
	@Output() onEmitValue: EventEmitter<any> = new EventEmitter();

	constructor(
		private emitValueService: EmitValueService,
	) {}

	ngOnInit() {
		this.emitValueService.value
			.pipe(takeUntil(this.ngUnsubscribe))
			.subscribe(value => this.onEmitValue.emit(value));
	}

	isExpanded(index: number) {
		return this.expandedItems.indexOf(index) > -1;
	}

 }
 