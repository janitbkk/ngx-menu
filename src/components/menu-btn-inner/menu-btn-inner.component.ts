import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { MenuItem, Icons } from '../../models/index';
import { 
	getIcon,
	isSvgIcon,
	isMatIcon
} from '../../helpers/index';

 @Component({
	selector: 'cdl-menu-btn-inner',
	templateUrl: './menu-btn-inner.component.html',
	styleUrls: ['./menu-btn-inner.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
 })
 export class MenuBtnInnerComponent {
	@Input() item: MenuItem;

	get icon(): Icons {
		return getIcon(this.item);
	}

	get isSvgIcon(): boolean {
		return isSvgIcon(this.icon);
	}

	get isMatIcon(): boolean {
		return isMatIcon(this.icon);
	}

}
 