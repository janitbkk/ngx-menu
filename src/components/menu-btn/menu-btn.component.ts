import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { EmitValueService } from '../../services/emit-value.service';
import { MenuItem, TriggerFunction, EmitValue, Actions } from '../../models/index';
import { 
	getAction, 
	isEmitValue, 
	isExternalLink, 
	isInternalLink, 
	isTriggerFunction
} from '../../helpers/index';

 @Component({
	selector: 'cdl-menu-btn',
	templateUrl: './menu-btn.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
 })
 export class MenuBtnComponent {
	@Input() item: MenuItem;
	@Input() isDropdown: boolean = false;

	constructor(
		private emitValueService: EmitValueService
	) {}

	get isLink() {
		return this.isExternalLink || this.isInternalLink;
	}

	get action(): Actions {
		return getAction(this.item);
	}

	get isEmitValue(): boolean {
		return isEmitValue(this.action);
	}

	get isExternalLink(): boolean {
		return isExternalLink(this.action);
	}

	get isInternalLink(): boolean {
		return isInternalLink(this.action);
	}

	get isTriggerFunction(): boolean {
		return isTriggerFunction(this.action);
	}

	triggerFunction(action: TriggerFunction): void {
		action.value();
	}

	onEmitValue(action: EmitValue): void  {
		this.emitValueService.emit(action.value);
	}
}
 