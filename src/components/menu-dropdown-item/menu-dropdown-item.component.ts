import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { MenuItem } from '../../models/index';

@Component({
	selector: 'cdl-menu-dropdown-item',
	templateUrl: './menu-dropdown-item.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuDropdownItemComponent {
	@Input() item: MenuItem;
}
 